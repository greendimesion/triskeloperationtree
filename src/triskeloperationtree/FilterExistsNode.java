package triskeloperationtree;

import triskelTable.Table;

public class FilterExistsNode extends FunctionNode {

    public FilterExistsNode() {
        super();
    }

    @Override
    public Table execute() {
        Table firstChild = ((TableNode) childs.get(0)).getTable();
        Table secondChild = ((TableNode) childs.get(1)).getTable();
        return filterIfExists(firstChild, secondChild);
    }

    private Table filterIfExists(Table firstChild, Table secondChild) {
        String filterVariable = getFilterVariable(firstChild, secondChild);
        Table result = new Table(firstChild.getColumns());
        for (int indexFirstChild = 0; indexFirstChild < firstChild.size(); indexFirstChild++) {
            for (int indexSecondChild = 0; indexSecondChild < secondChild.size(); indexSecondChild++) {
                if (firstChild.get(indexFirstChild, filterVariable).equals(secondChild.get(indexSecondChild, filterVariable))) {
                    result.addRow(firstChild.getRow(indexFirstChild));
                }
            }
        }
        return result;
    }

    private String getFilterVariable(Table firstTable, Table secondTable) {
        for (String firstTableColumn : firstTable.getColumns()) {
            for (String secondTableColumn : secondTable.getColumns()) {
                if (firstTableColumn.equals(secondTableColumn)) {
                    return secondTableColumn;
                }
            }
        }
        return null;
    }

}
