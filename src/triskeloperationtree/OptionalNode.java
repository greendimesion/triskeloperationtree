package triskeloperationtree;

import triskelTable.Table;
import java.util.ArrayList;

public class OptionalNode extends FunctionNode {

    public OptionalNode() {
        super();
    }

    @Override
    public Table execute() {
        Table firstChild = ((TableNode) childs.get(0)).getTable();
        Table secondChild = ((TableNode) childs.get(1)).getTable();
        return optional(firstChild, secondChild);
    }

    private Table optional(Table firstChild, Table secondChild) {
        ArrayList<String> optionalColumns = getOptionalVariables(firstChild, secondChild);
        Table result = new Table(mergeColumns(firstChild.getColumns(), optionalColumns));
        Long[] firstChildRow, secondChildRow;
        for (int i = 0; i < firstChild.size(); i++) {
            firstChildRow = firstChild.getRow(i);
            secondChildRow = getOptionalValues(i,firstChild,secondChild, optionalColumns);
            result.addRow(mergeRows(firstChildRow, secondChildRow));
        }
        return result;
    }

    private ArrayList<String> getOptionalVariables(Table firstChild, Table secondChild) {
        ArrayList<String> optionalColumns = new ArrayList<>();
        for (String secondTableColumn : secondChild.getColumns()) {
            if (isOptional(secondTableColumn, firstChild)) {
                optionalColumns.add(secondTableColumn);
            }
        }
        return optionalColumns;
    }

    public boolean isOptional(String column, Table firstChild) {
        for (String firstTableColumn : firstChild.getColumns()) {
            if (firstTableColumn.equals(column)) {
                return false;
            }
        }
        return true;
    }

    private String[] mergeColumns(String[] firstChildColumns, ArrayList<String> optionalColumns) {
        String[] mergeResult = new String[firstChildColumns.length + optionalColumns.size()];
        System.arraycopy(firstChildColumns, 0, mergeResult, 0, firstChildColumns.length);
        int index = 0;
        for (int i = firstChildColumns.length; i < mergeResult.length; i++) {
            mergeResult[i]=optionalColumns.get(index);
            index++;
        }
        return mergeResult;
    }

    private Long[] getOptionalValues(int row, Table firstChild, Table secondChild, ArrayList<String> optionalColumns) {
        Long[] optionalRow = new Long[optionalColumns.size()];
        for (int i = 0; i < optionalRow.length; i++) {
            optionalRow[i]=null;
        }
        String[] firstChildColumns = firstChild.getColumns();
        String[] secondChildColumns = secondChild.getColumns();
        ArrayList<String> commonColumns = getCommonColumns(secondChildColumns, firstChildColumns);
        boolean isSameRow = true;
        for (int i = 0; i < secondChild.size(); i++) {
            for (String column : commonColumns) {
                if (!secondChild.get(i, column).equals(firstChild.get(row, column)))
                    isSameRow=false;
            }
            if (isSameRow){
                for (int j = 0; j < optionalRow.length; j++) {
                    optionalRow[j]=secondChild.get(i, optionalColumns.get(j));
                }
                break;
            }
            isSameRow = true;
        }
        return optionalRow;
    }

    private ArrayList<String> getCommonColumns(String[] secondChildColumns, String[] firstChildColumns) {
        ArrayList<String> commonColumns = new ArrayList<>();
        for (String secondChildColumn : secondChildColumns) {
            for (String firstChildColumn : firstChildColumns) {
                if (secondChildColumn.equals(firstChildColumn)) {
                    commonColumns.add(secondChildColumn);
                }
            }
        }
        return commonColumns;
    }

    private Long[] mergeRows(Long[] firstChildRow, Long[] secondChildRow) {
        Long[] result = new Long[firstChildRow.length + secondChildRow.length];
        System.arraycopy(firstChildRow, 0, result, 0, firstChildRow.length);
        System.arraycopy(secondChildRow, 0, result, firstChildRow.length, secondChildRow.length);
        return result;
    }

}
