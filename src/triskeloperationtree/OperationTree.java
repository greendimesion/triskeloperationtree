package triskeloperationtree;

public class OperationTree {
      
    private Node root;

    public OperationTree() {
        root = null;
    }

    public void setRoot(Node root) {
        if (this.root == null)
        this.root = root;
        else {
            FunctionNode functionNode = (FunctionNode) root;
            functionNode.addNode(this.root);
            this.root = root;
        }
    }

    public Node getRoot() {
        return root;
    }    
}
