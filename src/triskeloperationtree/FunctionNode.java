package triskeloperationtree;

import java.util.ArrayList;
import triskelTable.Table;

public abstract class FunctionNode extends Node{
    
    protected ArrayList<Node> childs;

    public FunctionNode() {
        this.childs = new ArrayList<>();
    }
    
    public void addNode(Node node){
        childs.add(0, node);
    }
    
    public Node getNode(int index){
        return childs.get(index);
    }
    
    public int getChildNumber(){
        return childs.size();
    }
    
    public void changeNode(int nodeIndex, Node newNode){
        childs.add(nodeIndex, newNode);
        childs.remove(nodeIndex+1);
    }
    
    public abstract Table execute();
}
