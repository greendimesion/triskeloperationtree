package triskeloperationtree;

import patterns.Pattern;


public class PatternNode extends Node{
    
    private Pattern pattern;

    public PatternNode(Pattern pattern) {
        this.pattern = pattern;
    }

    public Pattern getPattern() {
        return pattern;
    }
    
}
