package triskeloperationtree;

import triskelTable.Table;

public class JoinNode extends FunctionNode{

    public JoinNode() {
        super();
    }
    
    @Override
    public Table execute() {
        Table resultTable;
        resultTable = getBiggestTable();
//        resultTable = ((TableNode) childs.get(0)).getTable();
//        childs.remove(0);
        for (Node tableNode : childs) {
            resultTable = resultTable.join(((TableNode) tableNode).getTable());
        }
        return resultTable;
    }

    private Table getBiggestTable() {
        TableNode bigChild;
        int bigChildIndex;
        bigChild = (TableNode) childs.get(0);
        bigChildIndex = 0;
        for (int i = 1; i < childs.size(); i++) {
            if (bigChild.getTable().getColumns().length < 
                    ((TableNode)childs.get(i)).getTable().getColumns().length)
            {
                bigChild = (TableNode) childs.get(i);
                bigChildIndex = i;
            }
        }
        childs.remove(bigChildIndex);
        return bigChild.getTable();
    }
    
    
    
}
