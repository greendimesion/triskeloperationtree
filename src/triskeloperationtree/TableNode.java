package triskeloperationtree;

import triskelTable.Table;

public class TableNode extends Node{
    
    private Table table;

    public TableNode(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return table;
    }
    
}
