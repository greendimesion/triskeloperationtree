package triskeloperationtree;

import triskelTable.Table;

public class MinusNode extends FunctionNode {
    
    public MinusNode() {
        super();
    }

    @Override
    public Table execute() {
        Table firstChild = ((TableNode) childs.get(0)).getTable();
        Table secondChild = ((TableNode) childs.get(1)).getTable();
        return minus(firstChild, secondChild);
    }

    private Table minus(Table firstChild, Table secondChild) {
        String commonVariable = getCommonVariable(firstChild, secondChild);
        Table result = new Table(firstChild.getColumns());
        boolean exist = false;
        for (int indexFirstChild = 0; indexFirstChild < firstChild.size(); indexFirstChild++) {
            for (int indexSecondChild = 0; indexSecondChild < secondChild.size(); indexSecondChild++) {
                if (firstChild.get(indexFirstChild, commonVariable).equals(secondChild.get(indexSecondChild, commonVariable))) {
                    exist=true;
                    break;
                }
            }
            if (!exist)result.addRow(firstChild.getRow(indexFirstChild));
            exist=false;
        }
        return result;
    }

    private String getCommonVariable(Table firstTable, Table secondTable) {
        for (String firstTableColumn : firstTable.getColumns()) {
            for (String secondTableColumn : secondTable.getColumns()) {
                if (firstTableColumn.equals(secondTableColumn)) {
                    return secondTableColumn;
                }
            }
        }
        return null;
    }

}
