package triskeloperationtree;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import triskelTable.Table;

public class ExecutionTest {
    
    OperationTreeForest operationTreeForest;

    public ExecutionTest() {
        this.operationTreeForest = new OperationTreeForest();
    }
    
    @Test
    public void functionExecutionTest(){
        OperationTree operationTree = operationTreeForest.createDoubleTableTree();
        FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
        Table functionTable = functionNode.execute();
        Table resultTable = new Table(new String[]{"a","title","author"});
        assertTrue(functionTable.equals(resultTable));
    }
    
    @Test
    public void joinExecutionTest(){
        OperationTree operationTree = operationTreeForest.createDoubleTableTree();
        JoinNode joinNode = (JoinNode) operationTree.getRoot();
        Table joinTable = joinNode.execute();
        Table resultTable = new Table(new String[]{"a","title","author"});
        assertTrue(joinTable.equals(resultTable));
    }
    
    @Test
    public void filterExistsExecutionTest(){
        OperationTree operationTree = operationTreeForest.createFilterExistsTree();
        FilterExistsNode filterExistsNode = (FilterExistsNode) operationTree.getRoot();
        Table result = filterExistsNode.execute();
        assertTrue(result.size() == 4);
    }
    
    @Test
    public void OptionalExecutionTest(){
        OperationTree operationTree = operationTreeForest.createOptionalTree();
        OptionalNode optionalNode = (OptionalNode) operationTree.getRoot();
        Table result = optionalNode.execute();
        assertTrue(result.size() == 5);
        assertTrue(result.get(4, "author") == null);
    }
    
    @Test
    public void MinusExecutionTest(){
        OperationTree operationTree = operationTreeForest.createMinusTree();
        MinusNode minusNode = (MinusNode) operationTree.getRoot();
        Table result = minusNode.execute();
        assertTrue(result.size() == 1);
    }
    
    @Test
    public void  FilterNotExistsExecutionTest(){
        OperationTree operationTree = operationTreeForest.createFilterNotExistsTree();
        FilterNotExistsNode filterNotExistsNode = (FilterNotExistsNode) operationTree.getRoot();
        Table result = filterNotExistsNode.execute();
        assertTrue(result.size() == 1);
    }
}
