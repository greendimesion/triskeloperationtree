package triskeloperationtree;

import patterns.Constant;
import patterns.Pattern;

import triskelTable.Table;

public class OperationTreeForest {

    public OperationTreeForest() {
    }

    //        (joinN)[r]
    //            |
    //        (patternN)
    //         [1,2,3]
    public OperationTree createSimpleTree() {
        OperationTree operationTree = new OperationTree();

        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(1, 2, 3)));

        operationTree.setRoot(joinNode);
        return operationTree;
    }

    //             (joinN)[r]
    //                 |
    //     ________________________
    //     |           |          |
    // (patternN)  (patternN) (patternN)
    //   [7,8,9]    [4,5,6]    [1,2,3]
    public OperationTree createTriplePatternTree() {
        OperationTree operationTree = new OperationTree();

        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(1, 2, 3)));
        joinNode.addNode(new PatternNode(initPattern(4, 5, 6)));
        joinNode.addNode(new PatternNode(initPattern(7, 8, 9)));

        operationTree.setRoot(joinNode);
        return operationTree;
    }

    //             (joinN)[r]
    //                 |
    //     ________________________
    //     |                      |
    //  (tableN)               (tableN)
    //  ["a","author"]         ["a","title"]
    public OperationTree createDoubleTableTree() {
        OperationTree operationTree = new OperationTree();

        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new TableNode(new Table(new String[]{"a", "title"})));
        joinNode.addNode(new TableNode(new Table(new String[]{"a", "author"})));

        operationTree.setRoot(joinNode);
        return operationTree;
    }
    
    //         (FilterExistsN)[r]
    //                 |
    //     ________________________
    //     |                      |
    //  (tableN)               (tableN)
    //  ["a","author"]         ["a","title"]
    public OperationTree createFilterExistsTree() {
        OperationTree operationTree = new OperationTree();

        FilterExistsNode filterExistsNode = new FilterExistsNode();
        filterExistsNode.addNode(new TableNode(createTitleTable()));
        filterExistsNode.addNode(new TableNode(createAuthorTable()));

        operationTree.setRoot(filterExistsNode);
        return operationTree;
    }
    
    //        (FilterNotExistsN)[r]
    //                 |
    //     ________________________
    //     |                      |
    //  (tableN)               (tableN)
    //  ["a","title"]          ["a","author"]
     public OperationTree createFilterNotExistsTree() {
        OperationTree operationTree = new OperationTree();

        FilterNotExistsNode filterNotExistsNode = new FilterNotExistsNode();
        filterNotExistsNode.addNode(new TableNode(createAuthorTable()));
        filterNotExistsNode.addNode(new TableNode(createTitleTable()));

        operationTree.setRoot(filterNotExistsNode);
        return operationTree;
    }
    
    //            (OptionalN)[r]
    //                 |
    //     ________________________
    //     |                      |
    //  (tableN)               (tableN)
    //  ["a","title"]          ["a","author"]
     public OperationTree createOptionalTree() {
        OperationTree operationTree = new OperationTree();

        OptionalNode optionalNode = new OptionalNode();
        optionalNode.addNode(new TableNode(createAuthorTable()));
        optionalNode.addNode(new TableNode(createTitleTable()));

        operationTree.setRoot(optionalNode);
        return operationTree;
    }
     
    //            (MinusN)[r]
    //                 |
    //     ________________________
    //     |                      |
    //  (tableN)               (tableN)
    //  ["a","title"]          ["a","author"]
     public OperationTree createMinusTree() {
        OperationTree operationTree = new OperationTree();

        MinusNode minusNode = new MinusNode();
        minusNode.addNode(new TableNode(createAuthorTable()));
        minusNode.addNode(new TableNode(createTitleTable()));

        operationTree.setRoot(minusNode);
        return operationTree;
    }

    
    
    // ---------------------------------------
    // Values functions
    // ---------------------------------------
    
    private Pattern initPattern(int subject, int predicate, int Object) {
        return new Pattern(new Constant(subject), new Constant(predicate), new Constant(Object));
    }
    
    public Table createTitleTable() {
        Table result = new Table(new String[]{"a", "title"});
        result.addRow(new Long[]{1l, -120l});
        result.addRow(new Long[]{2l, -130l});
        result.addRow(new Long[]{8l, -119l});
        result.addRow(new Long[]{4l, -191l});
        result.addRow(new Long[]{7l, -291l});
        return result;
    }
    
    public Table createAuthorTable() {
        Table result = new Table(new String[]{"a", "author"});
        result.addRow(new Long[]{1l, 20l});
        result.addRow(new Long[]{2l, 30l});
        result.addRow(new Long[]{5l, 80l});
        result.addRow(new Long[]{8l, 19l});
        result.addRow(new Long[]{9l, 39l});
        result.addRow(new Long[]{4l, 91l});
        return result;
    }
}
