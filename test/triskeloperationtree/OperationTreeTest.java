package triskeloperationtree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import patterns.Constant;
import patterns.Pattern;
import triskelTable.Table;

public class OperationTreeTest {

    OperationTreeForest operationTreeForest;

    public OperationTreeTest() {
        this.operationTreeForest = new OperationTreeForest();
    }

    @Test
    public void analizeSimpleTree() {
        OperationTree operationTree = operationTreeForest.createSimpleTree();
        assertTrue(operationTree.getRoot() instanceof JoinNode);
        JoinNode joinNode = (JoinNode) operationTree.getRoot();
        assertEquals(1, joinNode.getChildNumber());
        assertTrue(joinNode.getNode(0) instanceof PatternNode);
        PatternNode patternNode = (PatternNode) joinNode.getNode(0);
        Constant subject = (Constant) patternNode.getPattern().getSubject();
        Constant predicate = (Constant) patternNode.getPattern().getPredicate();
        Constant Object = (Constant) patternNode.getPattern().getObject();
        assertEquals(1, subject.getId());
        assertEquals(2, predicate.getId());
        assertEquals(3, Object.getId());
    }

    @Test
    public void analizeTriplePatternTree() {
        OperationTree operationTree = operationTreeForest.createTriplePatternTree();
        assertTrue(operationTree.getRoot() instanceof JoinNode);
        JoinNode joinNode = (JoinNode) operationTree.getRoot();
        assertEquals(3, joinNode.getChildNumber());
        assertTrue(joinNode.getNode(0) instanceof PatternNode);
        assertTrue(joinNode.getNode(1) instanceof PatternNode);
        assertTrue(joinNode.getNode(2) instanceof PatternNode);
        
        PatternNode patternNode = (PatternNode) joinNode.getNode(2);
        Constant subject = (Constant) patternNode.getPattern().getSubject();
        Constant predicate = (Constant) patternNode.getPattern().getPredicate();
        Constant Object = (Constant) patternNode.getPattern().getObject();
        assertEquals(1, subject.getId());
        assertEquals(2, predicate.getId());
        assertEquals(3, Object.getId());
        
        patternNode = (PatternNode) joinNode.getNode(1);
        subject = (Constant) patternNode.getPattern().getSubject();
        predicate = (Constant) patternNode.getPattern().getPredicate();
        Object = (Constant) patternNode.getPattern().getObject();
        assertEquals(4, subject.getId());
        assertEquals(5, predicate.getId());
        assertEquals(6, Object.getId());
        
        patternNode = (PatternNode) joinNode.getNode(0);
        subject = (Constant) patternNode.getPattern().getSubject();
        predicate = (Constant) patternNode.getPattern().getPredicate();
        Object = (Constant) patternNode.getPattern().getObject();
        assertEquals(7, subject.getId());
        assertEquals(8, predicate.getId());
        assertEquals(9, Object.getId());
    }

    @Test
    public void analizeDoubleTableTree() {
        OperationTree operationTree = operationTreeForest.createDoubleTableTree();
        assertTrue(operationTree.getRoot() instanceof JoinNode);
        JoinNode joinNode = (JoinNode) operationTree.getRoot();
        assertEquals(2, joinNode.getChildNumber());
        assertTrue(joinNode.getNode(0) instanceof TableNode);
        assertTrue(joinNode.getNode(1) instanceof TableNode);
        Table firstTable = ((TableNode) joinNode.getNode(0)).getTable();
        Table secondTable = ((TableNode) joinNode.getNode(1)).getTable();
        assertTrue((firstTable.join(secondTable)).equals(new Table(new String[]{"a", "title", "author"})));
    }

    @Test
    public void checkSwitchNodeTest() {
        OperationTree operationTree = operationTreeForest.createSimpleTree();
        assertTrue(operationTree.getRoot() instanceof JoinNode);
        JoinNode joinNode = (JoinNode) operationTree.getRoot();
        assertEquals(1, joinNode.getChildNumber());
        assertTrue(joinNode.getNode(0) instanceof PatternNode);
        TableNode tableNode = new TableNode(new Table(new String[]{"a", "title"}));
        joinNode.changeNode(0, tableNode);
        assertTrue(joinNode.getNode(0) instanceof TableNode);
        assertTrue((((TableNode) joinNode.getNode(0)).getTable()).equals(new Table(new String[]{"a", "title"})));
    }


    private Pattern initPattern(int subject, int predicate, int Object) {
        return new Pattern(new Constant(subject), new Constant(predicate), new Constant(Object));
    }

}
